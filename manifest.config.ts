import { defineManifest } from "@crxjs/vite-plugin";
import packageJson from "./package.json";
const { version } = packageJson;

export default defineManifest(async () => ({
  manifest_version: 3,
  name: "hello-tvos",
  version: `0.0.0.1`,
  version_name: version,
  content_scripts: [
    {
      js: ["src/index.tsx"],
      matches: ["http://*/*", "https://*/*"],
    },
  ],
}));
