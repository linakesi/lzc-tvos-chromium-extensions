SRC_FILES := \
	$(shell find . -type f -name '*.*' | sed 's/ /\\ /g') \
	$(NULL)

PWD := $(pwd)

default: $(SRC_FILES)
	npm run build;
	mkdir -p out;
	chromium --pack-extension="dist/";
	mv dist.crx dist.pem out

.PHONY: default
