import { isInputElement, isTextareaElement } from "./util";
export default function registryWebRim() {
  initRim();
  document.addEventListener("visibilitychange", () => {
    if (document.visibilityState == "visible") {
      initRim();
    } else {
    }
  });
}
let newTextWs: null | WebSocket;
let sendEnterWs: null | WebSocket;
function initRim() {
  if (
    newTextWs?.readyState == WebSocket.OPEN ||
    sendEnterWs?.readyState == WebSocket.OPEN
  ) {
    newTextWs?.close();
    sendEnterWs?.close();
  }
  newTextWs = new WebSocket("ws://127.0.0.1:3002/web-rim/new-text");
  newTextWs.onmessage = (ev) => {
    const active = document.activeElement;
    if (!active) return;
    const input = isInputElement(active);
    const textarea = isTextareaElement(active);
    if (!input && !textarea) return;
    const ele = active as any;
    ele.value = ev.data;
    ele.dispatchEvent(new Event("input", { bubbles: true }));
  };
  sendEnterWs = new WebSocket("ws://127.0.0.1:3002/web-rim/send-enter");
  sendEnterWs.onmessage = () => {
    const enterDownEvent = new KeyboardEvent("keydown", {
      key: "Enter",
      code: "Enter",
      keyCode: 13,
      which: 13,
      bubbles: true,
    });
    const enterUpEvent = new KeyboardEvent("keyup", {
      key: "Enter",
      code: "Enter",
      keyCode: 13,
      which: 13,
      bubbles: true,
    });
    document.activeElement?.dispatchEvent(enterUpEvent);
    document.activeElement?.dispatchEvent(enterDownEvent);
    document.dispatchEvent(enterUpEvent);
    document.dispatchEvent(enterDownEvent);
  };
}
