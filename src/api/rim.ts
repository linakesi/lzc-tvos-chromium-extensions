import axios from "axios";

const ll = axios.create({ baseURL: "http://127.0.0.1:3002" });

export async function setInputText(text: string) {
  return await ll.post("/rim-set-input-text", { text: text });
}
export async function emitInputBlur() {
  return await ll.post("/emit-input-blur");
}
export async function emitInputFocus() {
  return await ll.post("/emit-input-focus");
}
