export function isInputElement(ele: Element): HTMLInputElement | null {
  if (ele.tagName != "INPUT") return null;
  const input = ele as HTMLInputElement;
  return input;
}

export function isTextareaElement(ele: Element): HTMLTextAreaElement | null {
  if (ele.tagName != "TEXTAREA") return null;
  const input = ele as HTMLTextAreaElement;
  return input;
}
